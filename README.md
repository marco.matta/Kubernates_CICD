# Overview
[ConnectPetsTo.Me](http://www.connectpetsto.me) is a website that allows visitors to find pets that they might want to adopt, or a shelter where they might like to volunteer.

# [Project's Class Page](https://www.cs.utexas.edu/users/downing/cs373/projects/IDB.html)

# [API Documentation](https://documenter.getpostman.com/view/4693218/RWEjqHim#a50b578c-c9f6-4457-9695-c68c26f37cf7)

# [Technical Specification](https://docs.google.com/document/d/1i6461flSKyNWnF6pArVEBhRB87cRE7VhJwnB0YfxQkg/edit?usp=sharing)


# --------------------------------------------------------------------------------

# README: Class requirement section
##   Name, EID, and GitLab ID, of all members
    * Abdullah Abualsoud (AJ),    aa73228,    ajipsum
    * Alex Flores Escacega,       af28459,    afloresescarcega
    * David Mao,                  dm46452,    the_color_blurple
    * Jeffrey Zhu,                jwz269,     jeffwzhu
    * William Wesley Monroe,      wwm394,     purpleHey
    * Yoshinobu Nakada,           yn2536,     yoshi1579

## Git SHA
    0b3c7668cddb9c9faa0914697a674721b525db46

## Link to website:
[ConnectPetsTo.Me](http://www.connectpetsto.me)

## Estimated completion time (hours: int)
    * Abdullah Abualsoud (AJ) : 40
    * Alex Flores Escacega    : 20
    * David Mao               : 20
    * Jeffrey Zhu             : 25
    * Wesley Monroe           : 10
    * Yoshinobu Nakada        : 20

## Actual completion time (hours: int)
    * Abdullah Abualsoud (AJ) : 84
    * Alex Flores Escacega    : 35
    * David Mao               : 30
    * Jeffrey Zhu             : 30
    * William Wesley Monroe   : 10
    * Yoshinobu Nakada        : 25

## Comments
    * David Mao : We decided to make the web pages with React, and to generate pages by interacting with data that mimics what we expect to get from our backend database.
    This led to our first phase taking a lot longer than we probably expected, but hopefully it will pay off in the long run.
