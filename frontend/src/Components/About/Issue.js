import React, { Component } from 'react'

export default class Issue extends Component {
  constructor(props) {
    super(props)
    this.state= {
      'issue_count': 0,
    }
  }

  componentWillMount() {
    const url = 'https://gitlab.com/api/v4/projects/7164816/issues?author_id='
    fetch (url.concat(this.props.user, '&per_page=100'), {
    }).then(res => res.json())
  .then(response => response.length)
  .then(data => {
    this.setState({issue_count: data});
    this.props.increment(data);
    })
  }

  render(){
    return (
      <li className="list-group-item">Issues: {this.state.issue_count}</li>
    );
  }
}