import davidimg from './images/david.jpg'
import ajimg from './images/aj.jpg'
import wesleyimg from './images/wesley.png'
import aleximg from './images/alex.jpg'
import jeffreyimg from './images/jeffrey.png'
import yoshiimg from './images/yoshi.jpg'

export default function fetchData(name)
{
  let temp = {};

  let david =
  {
      "name": "David Mao",
      "bio": "Responsible for frontend. Student at UT. Loves both dogs and cats but not buildings.",
      "image": davidimg,
      "userId": '2440012',
  };

  let yoshi = 
  {
    "name": "Yoshinobu Nakada",
    "bio": "Responsible for frontend and deployment. Student at UT, soon to be graduate of UT.",
    "image": yoshiimg,
    "userId": '2458894',
  };

  let aj = 
  {
    "name": "AJ Soud",
    "bio": "Responsible for backend, deployment. Former combat medic, current UT student.",
    "image": ajimg,
    "userId": '2431421',
  };

  let alex = 
  {
    "name": "Alex Flores Escarcega",
    "bio": "Rising senior at UT. Responsible for frontend. I love coding and I love buildings, we should learn how to code buildings!",
    "image": aleximg,
    "userId": '2480227',
  };

  let jeffrey = 
  {
    "name": "Jeffrey Zhu",
    "bio": "Responsible for backend. Junior at UT. Loves cats, but doesn't own one.",
    "image": jeffreyimg,
    "userId": '2472072',
  };

  let wesley = 
  {
    "name": "W. Wesley Monroe",
    "bio": "Responsible for design and backend. Loves biking, bluegrass, and herding cats.",
    "image": wesleyimg,
    "userId": '2438307',
  }

  switch (name)
  {
    case 'david.mao.92@gmail.com':
      temp = david; break;
    case 'vivayoshivitss1333@gmail.com':
      temp = yoshi; break;
    case 'soud@utexas.edu':
      temp = aj; break;
    case 'jeffzhu123@utexas.edu':
      temp = jeffrey; break;
    case 'alex.floresescar@gmail.com':
      temp = alex; break;
    case 'wmonroe@cs.utexas.edu':
      temp = wesley; break;
    default:
      temp = 'missing';
  }

  return temp;
}
