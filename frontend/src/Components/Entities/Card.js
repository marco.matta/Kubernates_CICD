import React, { Component } from 'react';
import './Card.css';

class CardHeader extends React.Component{
	render(){
		return(
			<div className="cardHeader">
				<h2>{this.props.info.header[0]}</h2>
				<h5>{this.props.info.header[1]}</h5>
				<h5>{this.props.info.header[2]}</h5>
			</div>
			);
	}
}

class CardBody extends React.Component{
	render(){
		
		return(
			<div className="cardBody">
			{this.props.modelData}
			</div>
			);
	}
}

class Card extends React.Component{
	render(){
		let comps = this.props.modelData.map(data => {
			return(
				<div></div>
			);
		});
		
		return(
			<div className="card">
				<CardHeader header={this.props.info.header}/>
				<CardBody body={this.props.info.body}/>
			</div>
			);
	}
}

export default Card
