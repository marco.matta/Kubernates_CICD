import React, { Component } from 'react';
import './PetEntity.css';
import { connect } from 'react-redux';
import { fetchPetEntity } from '../../../actions/dataModelAction';
import EntityAttribute from '../EntityAttribute/EntityAttribute';
import RatingStar from '../../RatingStar/RatingStar';

// import Card from './Card';


class PetEntity extends Component{

    componentWillMount(){
        this.props.fetchPetEntity(this.props.match.params.petId);
        console.log(this.props.match.params);
        
    }

    render(){
        let info = this.props.petInfo;
        // let image = {
        //     backgroundImage: 'url('+this.props.petInfo.img+')',
        //     backgroundSize: '80%',
        // };
        let image = {
            background: 'linear-gradient(to right, #020202 0%,#020202 10%,  transparent 50%, transparent), url('+this.props.petInfo.img+') no-repeat center',
            backgroundSize: 'cover, 100% auto'
        };
        let entityAttribute = [];
        for (const key in this.props.petInfo){
            if(key != 'img'){
                entityAttribute.push(
                    <EntityAttribute attrKey = {key} attrValue = {info[key]}/>
                );
            }
        }
        
        return (
            <div>     
                <div className="jumbotron p-3 p-md-5 text-white rounded bg-dark" style={image}>
                    <div className ="jumbotron_wrapper container">
                        <div className="col-md-6 px-0"> 
                            <h1 className="display-4 font-italic text-white">{this.props.petInfo.name}</h1>
                            <p className="lead my-3 text-white">{this.props.petInfo.breed}</p>
                            <p className="lead my-3 text-white">Age: {this.props.petInfo.age}</p>
                            <p className="lead my-3 text-white">{this.props.petInfo.location}</p>
                        </div>
                    </div>
                </div>

                <main role="main" className="container">
                    <div className="row">
                        <div className="col-md-8 blog-main">
                            <div className="row mb-2">
                                <div className="col-md-12">
                                    <div className="card flex-md-row mb-4 box-shadow">
                                        <div className="card-body d-flex flex-column align-items-start">
                                            <h2 className="d-inline-block mb-2 text-primary"><b>About</b></h2>
                                    
                                            <div className="mb-1 text-muted">9 miles away</div>
                                            <br/>
                                            {entityAttribute}
                                        </div>
                                        
                                    </div>
                                </div>
                        
                            </div>
                            
                            <nav className="blog-pagination">
                                <a className="btn btn-outline-primary" href="#">Previous Pet</a>
                                <a className="btn btn-outline-secondary disabled" href="#">Next Pet</a>
                            </nav>

                        </div>

                        
                        {/* <aside className="col-md-4 blog-sidebar">
                            <div className="p-3 mb-3 bg-light rounded">
                                <h4 className="font-italic">About</h4>
                                <p className="mb-0">Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
                            </div>

                            <div className="p-3">
                                <h4 className="font-italic">Archives</h4>
                                <ol className="list-unstyled mb-0">
                                <li><a href="#">March 2014</a></li>
                                <li><a href="#">February 2014</a></li>
                                <li><a href="#">January 2014</a></li>
                                <li><a href="#">December 2013</a></li>
                                <li><a href="#">November 2013</a></li>
                                <li><a href="#">October 2013</a></li>
                                <li><a href="#">September 2013</a></li>
                                <li><a href="#">August 2013</a></li>
                                <li><a href="#">July 2013</a></li>
                                <li><a href="#">June 2013</a></li>
                                <li><a href="#">May 2013</a></li>
                                <li><a href="#">April 2013</a></li>
                                </ol>
                            </div>

                            <div className="p-3">
                                <h4 className="font-italic">Elsewhere</h4>
                                <ol className="list-unstyled">
                                <li><a href="#">GitHub</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Facebook</a></li>
                                </ol>
                            </div>
                        </aside>  */}

                    </div>

                </main>
                <div className="row mb-2">
                    <div className="col-md-6">
                    <div className="card flex-md-row mb-4 box-shadow h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                        <strong className="d-inline-block mb-2 text-primary">Vets</strong>
                        <h3 className="mb-0">
                            <a className="text-dark" href="#">Emancipet</a>
                        </h3>
                        <div className="mb-1 text-muted">9 miles away</div>
                        <RatingStar/>
                        <br/>
                        <p className="card-text mb-auto">“Emancipet offers No Cost and Low Cost spay and neutering services.”</p>
                        <a href="#">See all reviews</a>
                        </div>
                        <img className="card-img-right flex-auto d-none d-lg-block"  src="https://s3-media3.fl.yelpcdn.com/bphoto/wLB-AA3Dt15HeHs1OOxGGQ/ls.jpg" alt="Card image cap"/>
                    </div>
                    </div>
                    <div className="col-md-6">
                    <div className="card flex-md-row mb-4 box-shadow h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                        <strong className="d-inline-block mb-2 text-success">Shelter</strong>
                        <h3 className="mb-0">
                            <a className="text-dark" href="#">Austin Dog Rescue</a>
                        </h3>
                        <div className="mb-1 text-muted">3 miles away</div>
                        <RatingStar/>
                        <br/>
                        <p className="card-text mb-auto">Austin, TX 78798 Downtown</p>
                        <p className="card-text mb-auto">Contact: (512) 827-9787</p>

                        <a href="#">More Detail</a>
                        </div>
                        <img className="card-img-right flex-auto d-none d-lg-block" src="https://s3-media4.fl.yelpcdn.com/bphoto/OlIVz1LAijuaOs58mItYJA/o.jpg" alt="Card image cap"/>
                    </div>
                    </div>
                </div>
            </div>


        );
    }
}

const mapStateToProps = state => ({
    petInfo: state.petInfo.items
})

export default connect(mapStateToProps, {fetchPetEntity})(PetEntity)
