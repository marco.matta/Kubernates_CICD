import React, { Component } from 'react'
import './Review.css'
import RatingStar from '../../RatingStar/RatingStar';

export default class Review extends Component {
  render() {
    
    // console.log(this.props.reviewName);
    var r = this.props.reviewName;
    
    for(let i in r){
        // console.log(r['img']);
        var img = r['img'];
        var name = r['name'];
        var comment = r['comment'];
        var rating = r['rating'];
    }
    // console.log(img);
    
    return (
        <div className="row">
            <div className="col-md-12">
                <img className="reviewImage" src={img} alt="Card cap"/>
                <h2>{name}</h2> 
                <div className="row">
                    <div className="ratingArea">
                        <RatingStar rating={rating}/>
                    </div>
                </div>
                <br/>
                <p>{comment}</p>
            </div>
        </div>
    )
  }
}
