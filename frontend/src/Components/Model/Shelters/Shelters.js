import React, {Component} from 'react';
import {connect} from 'react-redux';
import ShelterItems from './ShelterItems'
import { fetchData } from '../../../actions/dataModelAction';
import './Shelters.css';

class Shelters extends Component{

  // TO DO LIST
  // fetch data from backend based
  // this.props.petType will indicate either dog or cat etc
  // this.props.petType will be used for filtering

  // this is getting hard coded static data for phase 1
  // check dataModelAction.js
  componentWillMount(){
    this.props.fetchData('shelter');
  }

  render(){
    let shelterItems = this.props.shelters.map(shelter => {
      return(
        <ShelterItems shelterData = {shelter}/>
      );
    });
    return (
      <div className="row">
        {shelterItems}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  shelters: state.shelters.items
});

export default connect(mapStateToProps, { fetchData })(Shelters)
