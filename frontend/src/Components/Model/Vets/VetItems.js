import React, { Component } from 'react'
import RatingStar from '../../RatingStar/RatingStar';
import { Link } from 'react-router-dom'

export default class VetItems extends Component {
  render() {
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}><img className="card-img-top" src={this.props.vetData.image} alt="Card cap"/></Link>
          <div className="card-body">
            <Link to={"/Vets/VetEntity/".concat(this.props.vetData.id)}><p className="card-text">{this.props.vetData.name}</p></Link>
            {/*<small className="text-muted">9 miles away</small>*/}
            <RatingStar rating= {this.props.vetData.rating}/>
            <br/>
            <div className="d-flex justify-content-between align-items-center">
            
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.location}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.vetData.contact}</button>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    )
  }
}
