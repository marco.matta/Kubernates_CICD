import React, { Component } from 'react'

export default class RatingStar extends Component {
  render() {
    let color={
      color: 'orange'
    };

    let stars = [];
    if (this.props.rating !== 0){
      for (let i = 0; i < 5; i++){
        if(i < this.props.rating){
          stars.push(
            <span className="fa fa-star checked" style={color}></span>
          );
        }
        else{
          stars.push(
            <span className="fa fa-star"></span>
          );
        }
      }
    }

    return (
    <div className="d-flex">
        {/* <span class="fa fa-star" style={color}></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star checked"></span>
        <span class="fa fa-star"></span>
        <span class="fa fa-star"></span> */}
        {stars}
    </div>
    )
  }
}
